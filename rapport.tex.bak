\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{makeidx}
\usepackage{datetime}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{lscape}
\usepackage{sectsty}
\usepackage{fancyhdr} 
\usepackage{enumitem}
\usepackage{amssymb}
\usepackage{sidecap}
\usepackage{xcolor}
\usepackage{lastpage}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{colortbl}
\linespread{1.1}
\usepackage{ragged2e}

%prog
\usepackage{listings}
\lstloadlanguages{Ruby}
\lstloadlanguages{Perl}
\lstset{%
numbers=left,
stepnumber=1,    
firstnumber=1,
numberfirstline=true,
basicstyle=\ttfamily\color{black},
commentstyle = \ttfamily\color{red},
keywordstyle=\ttfamily\color{blue},
stringstyle=\color{orange}}

% dimension de la page
\geometry{top=3cm, bottom=4cm, left=2.5cm, right=2.5cm}

% header et footer
\newdate{date}{12}{02}{2016}
\date{\displaydate{date}}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{1pt}
\renewcommand\footrulewidth{1pt}
\fancyfoot[L]{Avcar Baltic Champain\\}
\fancyfoot[R]{\today}
\fancyfoot[C]{Projet SCAL\\
\textbf{Page \thepage/\pageref{LastPage}}
}

\graphicspath{ {images/} }

\newtheorem{de}{Définition}[subsection] 
\newtheorem{theo}{Théorème}[section]    
\newtheorem{prop}[theo]{Proposition}   



\title{Optimisation de serveurs LAMP
  \\et problématique de scalabilité}

\begin{document}
\maketitle
\center
IUT Nancy-Charlemagne\\
Licence Professionnelle ASRALL

\vspace{2cm}

\begin{figure}[h]
\includegraphics[width=6cm]{logouni}
\centering
\end{figure}

\vspace{0.5cm}

\begin{figure}[h]
\includegraphics[width=6cm]{logoiut}
\centering
\end{figure}

\vspace{1cm}
\center
Avcar Osman, Baltic Husref, Champain Bastien

\newpage
\tableofcontents
\newpage

\section{Introduction}

\subsection{Présentation de la problématique}
\justify
Pour notre projet, l'objectif est d'étudier un serveur LAMP afin de proposer une optimisation et de répondre à une problématique de scalabilité.\\
Pour réussir notre projet, il faut étudier le domaine de la disponibilité pour les plateformes web, les technologies qui existent pour gérer la répartition de charge et le gestion de cache.\\
\justify
Nous devons également étudier les différentes solutions de benchmarks de plateformes web existantes afin d'anticiper les évolutions de l'architecture proposée.\\
\justify
Nous aborderont également les notions de capacity planning et d'engagement des services (SLA).\\
\justify
LAMP est un ensemble de logiciels libres permettant la construction de serveur de sites webs.\\
L signifie Linux (le système d'exploitation)\\
A signifie Apache (le serveur web)\\
M signifie MySQL ou MariaDB (le serveur de base de données)\\
P signifie PHP, Perl ou Python (les langages de script)

\subsection{Optimisation}
\justify
Par défaut lors de l'installation de LAMP, ce dernier n'est pas optimisé. Afin d'utiliser au mieux les ressources du serveur, il est recommandé d'optimiser son serveur afin d'utiliser au mieux celles-ci. En optimisant son serveur LAMP, les performances du serveur augmenteront et les sites webs hébergés sur le serveur pourront recevoir plus de clients et la navigation sera plus fluide. La performance d'un serveur est importante, elle peut influancer le référencement d'un site sur un moteur de recherche, par exemple le temps de réponse du serveur est pris en compte.

\subsection{Haute disponibilité}
\justify
La haute disponibilité va permettre d'augmenter le taux de disponibilité du serveur. Il faut rendre les sites web accessibles le plus longtemps possible. Pour cela on peut mettre en place des réplications de serveur apache et base de données. Par exemple si on a 2 serveurs identiques, le risque que les sites web soient inaccessibles est réduit car si un serveur subit une panne, le 2nd prendra le relais, la haute disponibilité est donc augmentée. 

\section{État du serveur web}
\subsection{Description technique du serveur}
\justify
Processeur :\\
AMD Opteron™ 6338P\\
12 cœurs x 2,3 GHz

\justify
Mémoire vive :\\
32 Go de RAM\\
DDR3 ECC

\justify
Disque dur :\\
2 x 2 To SATA 

\subsection{Versions}
\justify
Version système d'exploitation :\\
Linux s18617385 3.16.0-4-amd64 \#1 SMP Debian 3.16.7-ckt11-1+deb8u6 (2015-11-09) x86\_64 GNU/Linux

\justify
Version Apache :\\
Apache/2.4.10 (Debian)

\justify
Version Mysql :\\
mysql  Ver 14.14 Distrib 5.5.46, for debian-linux-gnu (x86\_64) using readline 6.3

\justify
Version PHP :\\
PHP 5.6.17-0+deb8u1 (cli) (built: Jan 13 2016 09:10:12)\\
Copyright (c) 1997-2015 The PHP Group\\
Zend Engine v2.6.0, Copyright (c) 1998-2015 Zend Technologies\\
with Zend OPcache v7.0.6-dev, Copyright (c) 1999-2015, by Zend Technologies

\justify
Site basé sur le CMS Joomla 1.5.

\subsection{Modules apache}
\justify
 core\_module (static)
 so\_odule (static)\\
 watchdog\_module (static)\\
 http\_module (static)\\
 log\_config\_module (static)\\
 logio\_module (static)\\
 version\_module (static)\\
 unixd\_module (static)\\
 access\_compat\_module (shared)\\
 actions\_module (shared)\\
 alias\_module (shared)\\
 auth\_basic\_module (shared)\\
 authn\_core\_module (shared)\\
 authn\_file\_module (shared)\\
 authz\_core\_module (shared)\\
 authz\_host\_module (shared)\\
 authz\_user\_module (shared)\\
 autoindex\_module (shared)\\
 cache\_module (shared)\\
 cache\_disk\_module (shared)\\
 deflate\_module (shared)\\
 dir\_module (shared)\\
 env\_module (shared)\\
 expires\_module (shared)\\
 filter\_module (shared)\\
 headers\_module (shared)\\
 mime\_module (shared)\\
 mpm\_prefork\_module (shared)\\
 negotiation\_module (shared)\\
 pagespeed\_module (shared)\\
 php5\_module (shared)\\
 rewrite\_module (shared)\\
 setenvif\_module (shared)\\
 status\_module (shared)
 \subsection{Configuration réseau}
 \justify
 eth0 :\\
 ipv6 : 2001:08D8:087D:3100:0000:0000:006B:D53D\\
 masque : FFFF:FFFF:FFFF:FFFF:0000:0000:0000:0000\\
 passerelle : 10.255.255.1
 
\section{Cahier des charges}
\subsection{Présentation du projet}
\subsubsection{Objet du projet}
\justify
Pour le projet, l'objectif est l'optimisation de serveur LAMP et problématique de scalabilité. Notre tuteur nous a mis à disposition un serveur web ayant un site avec un trafic réel, le site est \underline {http://www.trackmusik.fr}.\\
Dans une premier temps, nous mettons en place l'optimisation du serveur web, puis dans un second temps nous verrons la haute disponibilité.
\subsubsection{Composition de l'équipe}
\justify
L'équipe est composée des étudiants suivants :\\
- Avcar Osman\\
- Baltic Husref\\
- Champain Bastien

\justify
Le tuteur est Yohan Parent.
\subsubsection{Délai}
\justify
Nous avons commencé le projet le 25 Janvier 2016, et le rapport final sera à rendre pour le 28 Mars 2016.
\subsection{Suivi du projet}
\justify
Pour le suivi du projet, nous renseignons à chaque fin de séance ce que l'on a fait sur la page \underline {http://suivi-projet-asrall.iutnc.univ-lorraine.fr/}. Nous communiquons tous les jours entre nous, et nous envoyons fréquemment des mails au tuteur. Chaque vendredi nous faisons une réunion avec le tuteur afin de voir où on en est, et de lui poser des questions sur certains points.
\section{Tâches réalisées}
\justify
Pour l'instant, nous nous sommes documenté sur l'optimisation de serveur LAMP, les possibilités d'optimisation pour apache et mysql, sur les MPM prefork et worker.\\
Nous avons étudié le serveur, ses versions, et ce qui était installé dessus.\\
Nous avons étudié et réalisé divers benchmarks afin d'analyser les performances actuelles du serveur.\\
Nous avons essayé de passer du MPM prefork vers le MPM worker.\\
Nous avons testé divers paramètres dans le fichier de configuration apache afin d'optimiser apache.\\
Nous avons commencé à rédiger un guide d'optimisation pour serveur LAMP.\\
Nous avons installé le module Google PageSpeed, mais celui ci pose encore des problèmes à l'heure actuelle.\\
Du côté de MySQL, nous nous sommes documenté sur la manière d'optimiser MySQL\\
Nous avons permis aux logs de requêtes lente de se loger sans les indexes dans la configuration de MySQL.\\
Nous avons défragmenté toutes les tables de la base de données.\\
Nous avons réalisé des tests de performances sur MySQL et analysé les résultats.
\section{Tâches à réaliser}
\justify
Il reste des tâches à réaliser : \\
- Optimisation d'apache à finir\\
- Optimisation de MySQL à finir\\
- Mettre en place une solution de haute-disponibilité\\
- Mettre en place la répartition de charge\\
- Etudier dareboost










%fin du document
\end{document}
