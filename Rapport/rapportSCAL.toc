\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Pr\IeC {\'e}sentation du projet}{4}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Probl\IeC {\'e}matique}{4}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Organisation}{4}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Contexte de mise en place des optimisations (e-commerce)}{5}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Contexte du projet}{5}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Serveur LAMP}{6}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Description technique du serveur}{8}{subsubsection.1.2.3}
\contentsline {paragraph}{Version Apache :}{8}{section*.2}
\contentsline {paragraph}{Version Mysql :}{8}{section*.3}
\contentsline {paragraph}{Version PHP :}{8}{section*.4}
\contentsline {paragraph}{Version Joomla :}{9}{section*.5}
\contentsline {subsubsection}{\numberline {1.2.4}Modules apache}{9}{subsubsection.1.2.4}
\contentsline {subsubsection}{\numberline {1.2.5}Configuration r\IeC {\'e}seau}{10}{subsubsection.1.2.5}
\contentsline {subsection}{\numberline {1.3}Tests de performance du serveur}{10}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Disques Durs}{10}{subsubsection.1.3.1}
\contentsline {paragraph}{Lecture}{10}{section*.6}
\contentsline {paragraph}{Ecriture}{11}{section*.7}
\contentsline {subsubsection}{\numberline {1.3.2}R\IeC {\'e}seau}{11}{subsubsection.1.3.2}
\contentsline {subsection}{\numberline {1.4}Optimisation}{12}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}L'optimisation de serveur}{12}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}L'importance d'une optimisation}{13}{subsubsection.1.4.2}
\contentsline {section}{\numberline {2}Cahier des charges}{14}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}sentation du projet}{14}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Objet du projet}{14}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Composition de l'\IeC {\'e}quipe}{14}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}D\IeC {\'e}lai}{15}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Suivi du projet}{15}{subsection.2.2}
\contentsline {section}{\numberline {3}Optimisation du serveur Web}{15}{section.3}
\contentsline {subsection}{\numberline {3.1}Environnement critique}{15}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Site web de e-commerce}{16}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Implication}{16}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Syst\IeC {\`e}me de gestion des contenus}{16}{subsubsection.3.2.2}
\contentsline {paragraph}{Joomla 3.4.8}{16}{section*.8}
\contentsline {paragraph}{Autre solution : Prestashop}{17}{section*.9}
\contentsline {subsection}{\numberline {3.3}Activit\IeC {\'e} et traffic sur un serveur web}{17}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Pr\IeC {\'e}paration de la plateforme}{17}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Backup}{17}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Plateforme de test}{17}{subsubsection.3.4.2}
\contentsline {subsection}{\numberline {3.5}Optimisation de la couche middleware}{18}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Optimisation Apache}{18}{subsubsection.3.5.1}
\contentsline {paragraph}{Module Apache}{18}{section*.10}
\contentsline {subparagraph}{Prefork}{18}{section*.11}
\contentsline {subparagraph}{Worker}{19}{section*.12}
\contentsline {subparagraph}{Compression gzip}{21}{section*.13}
\contentsline {subparagraph}{Temps d'expiration Expires}{22}{section*.14}
\contentsline {subparagraph}{Xcache PHP5}{23}{section*.15}
\contentsline {subparagraph}{Etags}{24}{section*.16}
\contentsline {subparagraph}{Timeout}{24}{section*.17}
\contentsline {subparagraph}{KeepAlive}{25}{section*.18}
\contentsline {subparagraph}{KeepAliveTimeout}{25}{section*.19}
\contentsline {subparagraph}{HostnameLookups}{25}{section*.20}
\contentsline {subparagraph}{Serveur Memcached}{25}{section*.21}
\contentsline {subsubsection}{\numberline {3.5.2}Optimisation Mysql}{27}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Les logs de requ\IeC {\^e}tes lentes}{28}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Ne pas utiliser d'index}{29}{subsubsection.3.5.4}
\contentsline {subsubsection}{\numberline {3.5.5}L'utilisation des requ\IeC {\^e}tes lentes}{30}{subsubsection.3.5.5}
\contentsline {subsubsection}{\numberline {3.5.6}D\IeC {\'e}fragmentation}{30}{subsubsection.3.5.6}
\contentsline {section}{\numberline {4}Benchmarks}{30}{section.4}
\contentsline {subsection}{\numberline {4.1}Outils de benchmarks}{31}{subsection.4.1}
\contentsline {section}{\numberline {5}Suivi d'activit\IeC {\'e}}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}SLA}{33}{subsection.5.1}
\contentsline {section}{\numberline {6}Infrastructure de Haute disponibilit\IeC {\'e}}{34}{section.6}
\contentsline {subsection}{\numberline {6.1}Sch\IeC {\'e}ma de l'infrastructure}{34}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}HAProxy}{34}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Installation et configuration}{35}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Configurations r\IeC {\'e}seaux des serveurs}{35}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Load Balancing}{37}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Configuration serveurs web}{40}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Test de la r\IeC {\'e}partition de charge}{41}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}MariaDB}{41}{subsection.6.8}
\contentsline {subsection}{\numberline {6.9}Gelra Cluster}{41}{subsection.6.9}
\contentsline {subsection}{\numberline {6.10}Heartbeat}{42}{subsection.6.10}
\contentsline {subsubsection}{\numberline {6.10.1}Installation et configuration d'Heartbeat}{42}{subsubsection.6.10.1}
\contentsline {subsection}{\numberline {6.11}Installation MariaDB et Galera Cluster}{43}{subsection.6.11}
\contentsline {subsubsection}{\numberline {6.11.1}Configuration Galera Cluster}{43}{subsubsection.6.11.1}
\contentsline {subsection}{\numberline {6.12}Test de la r\IeC {\'e}plication}{44}{subsection.6.12}
\contentsline {section}{\numberline {7}Conclusion}{45}{section.7}
\contentsline {section}{\numberline {8}Remerciements}{45}{section.8}
\contentsline {section}{\numberline {9}Annexes}{46}{section.9}
